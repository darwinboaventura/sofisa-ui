import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RadioboxComponent} from './radiobox.component';

@NgModule({
	declarations: [RadioboxComponent],
	imports: [
		CommonModule
	],
	exports: [RadioboxComponent]
})

export class RadioboxModule {}
