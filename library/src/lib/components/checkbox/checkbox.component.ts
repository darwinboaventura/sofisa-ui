import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'so-checkbox',
	templateUrl: './checkbox.component.html',
	styleUrls: ['./checkbox.component.scss']
})

export class CheckboxComponent {
	@Input() label: String = '';
	@Input() name: String = '';
	@Input() value: String = '';
	@Input() checked: Boolean = false;
	@Input() disabled: Boolean = false;
	
	@Output() hasChange: EventEmitter<string> = new EventEmitter();
	
	handleChange(e) {
		this.hasChange.emit(e.target.value);
	}
}
