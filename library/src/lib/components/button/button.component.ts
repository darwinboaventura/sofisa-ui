import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'so-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})

export class ButtonComponent {
	@Input() type: String = 'primary';
	@Input() text: String;
	@Output() hasClicked: EventEmitter<any> = new EventEmitter();
	
	handleClick(e) {
		e.preventDefault();
		
		this.hasClicked.emit(true);
	}
}
