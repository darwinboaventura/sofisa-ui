import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'so-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.scss']
})

export class InputComponent {
	@Input() label: String = '';
	@Input() value: String = '';
	@Input() placeholder: String = '';
	@Input() inputType: String = 'text';
	@Input() inputConfig: any = {
		type: '',
		disabled: false,
		validation: {
			type: '',
			message: ''
		},
		params: {
			left: '',
			right: ''
		}
	};
	
	@Output() hasChanged: EventEmitter<string> = new EventEmitter();
	
	handleChange(e) {
		this.hasChanged.emit(e.target.value);
	}
}
